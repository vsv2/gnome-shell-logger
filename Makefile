FILES=$(shell find -iname '*.js' -and -not \( -name 'run.js' -or -path ./third_party/\* \))

.PHONY: test check
help:
	@echo "Following targets are available:"
	@echo "\thelp      show this help (default action if no target specified)"
	@echo "\ttest      run unit tests"
	@echo "\tcheck     perform eslint checks on project code"
	@echo "\tbeautify  style the code with eslint"
	@echo "\tdep       download and setup external depencies"
	@echo "\tclean     remove all temporary files (*.rej, *.orig, *.porig, *~)"
	@echo "\trealclean remove all temporary files and external dependencies"

test:
	@$(if $(COVERAGE),GJS_COVERAGE_PREFIXES=$(shell pwd) GJS_COVERAGE_OUTPUT=.coverage,) $(firstword $(JASMINE) jasmine) $(if $(MAKE_TERMOUT),--color,--no-color) $(if $(V),--verbose,)
	@$(if $(COVERAGE),lcov -r .coverage/coverage.lcov "**/third_party/**/*" "**/spec/*" -o .coverage/coverage.lcov)

.coverage/coverage.lcov: COVERAGE=1
.coverage/coverage.lcov: test

html-coverage: .coverage/coverage.lcov
	@rm -rf coverage
	@$(firstword $(GEN_HTML) genhtml) --output-directory coverage --branch-coverage .coverage/coverage.lcov

clean:
	@find \( -name '*.rej' -or -name '*.orig' -or -name '*.porig' -or -name '*~' \) -exec rm -v '{}' \;
	@rm -rfv .coverage

realclean: clean
	@git submodule deinit --all
	@rm -rfv coverage

check:
	@$(firstword $(ESLINT) eslint) $(if $(FIX),--fix,) $(FILES)

beautify: FIX=1
beautify: check
